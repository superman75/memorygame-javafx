package boardgame.gui;

import boardgame.engine.GameEngine;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import java.util.Arrays;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class Controller {

    private GameEngine engine = new GameEngine(4);

    private final int SIZE = 4;

    private int level, found, attempts, timeElapsed;
    private int firstBtnNumber, secondBtnNumber;
    private boolean oneTileOpened = false;
    private static boolean gameLock = false;
    private boolean isGaming;
    private Timer timer;

    private final String[] btnImages = new String[]{"/png/1.png","/png/2.png","/png/3.png","/png/4.png","/png/5.png","/png/6.png","/png/7.png","/png/8.png"};

    // set the flag that shows if the button is shown or not
    private boolean[] btnFlags;

    // variable of the button list
    private static Button[] btnList;

    // variable that show random integer assigned to each button
    private int[] randomArray;
    @FXML
    BorderPane borderPane;
    @FXML
    VBox vbox;
    @FXML
    GridPane gridPane;
    @FXML
    Button startBtn;
    @FXML
    TextField levelField;
    @FXML
    TextField foundField;
    @FXML
    TextField attemptsField;
    @FXML
    TextField timeField;
    @FXML
    Button stopBtn;
    @FXML
    Button minusLevelBtn;
    @FXML
    Button plusLevelBtn;

    @FXML
    public void initialize() {

        // set background of game panel
        ImageView img = new ImageView("/png/background.jpg");
        borderPane.setStyle("-fx-background-image: url('/png/background.jpg');-fx-background-repeat: no-repeat;");

        // initialize the variables of game state
        stopBtn.setDisable(true);
        startBtn.setDisable(false);
        isGaming = false;
        level = 0;

        // add padding to the game gui
        int BUTTON_PADDING = 10;
        gridPane.setPadding(new Insets(BUTTON_PADDING));
        gridPane.setHgap(BUTTON_PADDING);
        gridPane.setVgap(BUTTON_PADDING);
        vbox.setPadding(new Insets(BUTTON_PADDING));

        // assign the size of every array variables
        btnList = new Button[SIZE*SIZE];
        btnFlags = new boolean[SIZE*SIZE];
        randomArray = new int[SIZE*SIZE];

        // generate the random number for each element button
        this.generateRandom();

        // create new button on each grid
        for(int r = 0 ; r < SIZE; r++) {
            for (int c = 0; c < SIZE; c++){
                btnList[r*SIZE+c] = new Button();
                gridPane.add(btnList[r*SIZE+c], c, r);
                btnList[r*SIZE+c].setPrefSize(500, 500);
                DropShadow shadow = new DropShadow();
                btnList[r*SIZE+c].setEffect(shadow);
                btnList[r*SIZE+c].setStyle("-fx-background-image: url('/png/back.png')");
                final int temp =  r*SIZE + c;
                btnList[r*SIZE+c].setOnAction(event -> onBtnClick(temp));
            }
        }


    }


    /**
     * start the game
     */
    public void startGame() {
        stopBtn.setDisable(false);
        startBtn.setDisable(true);
        minusLevelBtn.setDisable(true);
        plusLevelBtn.setDisable(true);
        foundField.setText("0");
        timeField.setText("0s");
        attemptsField.setText("0");
        found = 0;
        attempts = 0;
        isGaming = true;
        gameLock = false;

        Arrays.fill(btnFlags, false);
        for (Button btn: btnList){
            btn.setGraphic(null);
        }
        startTimer();

    }

    /**
     * end the game
     */
    public void stopGame() {
        stopBtn.setDisable(true);
        startBtn.setDisable(false);
        minusLevelBtn.setDisable(false);
        plusLevelBtn.setDisable(false);
        isGaming = false;
        timer.cancel();
    }

    /**
     * increase the game level
     */
    public void upLevel() {
        if (level<9) level++;
        levelField.setText(String.valueOf(level));
    }

    /**
     * decrease the game level
     */
    public void downLevel() {
        if (level>0) level--;
        levelField.setText(String.valueOf(level));
    }

    /**
     * start the game tracking time
     */
    private void startTimer(){
        timeElapsed = 0;
        timeField.setText("" + timeElapsed + "s");
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                timeField.setText("" + (++timeElapsed) + "s");
            }
        }, 1000, 1000);
    }

    /**
     * handle the element button click event
     *
     * @param btnNumber
     */
    private void onBtnClick(int btnNumber){
        if (isGaming){
            if(btnFlags[btnNumber] || gameLock) return;

            // update the attempt number
            attemptsField.setText(String.valueOf(++attempts/2));

            // show  the assigned image to clicked button
            Image img = new Image(getClass().getResourceAsStream(btnImages[randomArray[btnNumber]]));
            ImageView imageView = new ImageView(img);
            imageView.setFitHeight(btnList[btnNumber].getHeight()*0.8);
            imageView.setFitWidth(btnList[btnNumber].getWidth()*0.8);
            btnList[btnNumber].setGraphic(imageView);

            // when the first button is clicked
            if(!oneTileOpened){
                oneTileOpened = true;
                firstBtnNumber = btnNumber;
                btnFlags[firstBtnNumber] = true;
            // when the second button is clicked
            } else {
                oneTileOpened = false;
                secondBtnNumber = btnNumber;
                btnFlags[secondBtnNumber] = true;

                // if the first number and second number is different, the game will be locked for a while
                if(randomArray[firstBtnNumber] != randomArray[secondBtnNumber]){

                    gameLock = true;
                    IncorrectMatchThread incorrectMatchThread = new IncorrectMatchThread();
                    incorrectMatchThread.start();
                // if the two numbers are matched, you can continue the game
                } else {
                    foundField.setText(String.valueOf(++found));

                    // when you find all matched number, it will show success dialog
                    if (found*2==SIZE*SIZE){
                        stopGame();
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Success!");
                        alert.setHeaderText(null);
                        alert.setContentText("Well done, You solved it!");

                        alert.showAndWait();
                    }
                }
            }

        // if the game is not started and button is clicked, it will show the alert dialog.
        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Alert!");
            alert.setHeaderText(null);
            alert.setContentText("Please press start button!");

            alert.showAndWait();
        }
    }

    /**
     * generate random number for each button
     * Each two numbers will be paired.
     */
    private void generateRandom(){
        for(int i = 0; i < SIZE*SIZE; i++){
            randomArray[i] = i/2;
        }

        Random random = new Random();
        for (int i = 0; i < randomArray.length; i++){
            int randomPosition = random.nextInt(randomArray.length);
            int temp = randomArray[i];
            randomArray[i] = randomArray[randomPosition];
            randomArray[randomPosition] = temp;
        }
    }

    /**
     * New thread that will be activated when two clicked numbers are not matched.
     */
    class IncorrectMatchThread extends Thread {
        public void run() {

            try {
                Thread.sleep(2000 - 200*level);
                Platform.runLater(() -> {
                    System.out.println("Game is unlocked");
                    gameLock = false;
                    btnFlags[firstBtnNumber] = false;
                    btnFlags[secondBtnNumber] = false;
                    btnList[firstBtnNumber].setGraphic(null);
                    btnList[secondBtnNumber].setGraphic(null);
                    
                });
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
